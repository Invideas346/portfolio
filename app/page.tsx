function NavBar() {
	const entries = [
		{ label: "Home" },
		{ label: "About" },
		{ label: "Contact" }
	]
	return (
		<nav className="flex flex-row justify-center w-screen bg-[#232323] 
						text-white items-center h-14 backdrop-blur-sm">
			<ul className="flex flex-row justify-center space-x-10">
				{entries.map((entry) => {
					return (<li key={entry.label}>{entry.label}</li>)
				})}
			</ul>	
		</nav>
	);
}

export default function Home() {
  return (
    <main className="h-screen w-screen bg-[#121212]">
		<NavBar />
    </main>
  )
}
